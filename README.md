Utilitaire de conversion itslearning vers  moodle
---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Sommaire**  *généré avec [DocToc](https://github.com/thlorenz/doctoc)*

- [Présentation](#Présentation)
- [Usage](#Usage)
- [Avertissements](#Avertissements)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Présentation

Cet utilitaire est proposé par Soazig Charlot. Il permet la récupération assistée d'une partie des exercices itslearning. Les contenus compatibles sont convertis en questions pour l'activité "test" de moodle (Éléa, Magistère ou autre).


## Usage


1. Dans itslearning, exporter les questions d'un exercice. Vous pouvez suivre [ce tutoriel](https://conversion-itslearning-moodle-outil-numerique-da-e51fd4da95ccd8.forge.apps.education.fr/tutoPourExport.pdf) pour faire cet export en fonction de la version des exercices. En fonction de la version de vos exercices, vous obtiendrez soit un fichier zip soit un fichier xlsx
2. Téléverser le fichier obtenu sur l'utilitaire choisi ("parcourir" depuis l'ancien ou le nouvel exericeur) ici : https://conversion-itslearning-moodle-outil-numerique-da-e51fd4da95ccd8.forge.apps.education.fr/
3. Un fichier texte va être automatiquement généré. Saisir son nom (par defaut il a pour nom questions_and_answers.txt) et choisir l'emplacement pour l'enregistrer
4. Dans Elea, à partir d'un parcours, choisir "plus" puis "banque de questions". Ensuite dans la zone de liste, choisir "importer" et cocher "format GIFT". Sélectionner le fichier texte enregistré précédemment puis importer
5. [Ajouter une activité "test"](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Integrer%20des%20activites/Integrer_une_activite_Test/) puis dans l'onglet "Questions", ajouter les questions créées "depuis la banque de questions".

## Avertissements

L'utilitaire est fourni en l'état, sans garantie de bon fonctionnement ou de correctifs.
Parmi les limites connues : 
- Pour les textes à trou, seul l'intitulé de la question est récupéré. 
- La récupération des appariements n'est pas fonctionnelle.
- De manière générale, aucun média (image, son, vidéo) n'est exporté depuis itslearning. Ces contenus ne sont donc pas récupérables.
- la conversion ne se lance pas si un type de question non pris en charge est présent dans l'exercice.
- l'export produit comporte parfois des erreurs de formatage qui peuvent être bloquantes dans moodle.
- Pour les nouveaux exercices, il faut qu'au moins un utilisateur ait répondu à l'exercice pour pouvoir exporter les résultats (il est possible d'ajouter un collègue comme élève pour lui demander de fournir une série de réponses)