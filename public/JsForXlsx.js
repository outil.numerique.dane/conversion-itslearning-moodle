// Fonction pour lire le fichier et extraire les données
async function readXlsx() {
    const fileInput = document.getElementById('fileInputXlsx');
    const file = fileInput.files[0];
    console.log(file);

    if (!file) {
        alert('Please select a file first!');
        return;
    }

    const reader = new FileReader();
    reader.onload = function (e) {
        const data = new Uint8Array(e.target.result);
        const workbook = XLSX.read(data, { type: 'array' });
        generateGiftFile(workbook)

    }

    //  saveToFile(fichierResultat);
    reader.readAsArrayBuffer(file);
}



function generateGiftFile(workbook) {


    const sheetName = workbook.SheetNames[1];
    const worksheet = workbook.Sheets[sheetName];

    // Décoder la plage de cellules utilisée dans la feuille
    const range = XLSX.utils.decode_range(worksheet['!ref']);
    let giftContent = '';

    // Parcourir chaque ligne dans la plage
    for (let row = range.s.r + 1; row <= range.e.r; ++row) { // Ignorer la première ligne (en-tête)
        const questionCellAddress = { c: 4, r: row }; // Colonne E (index 4)
        const correctAnswerCellAddress = { c: 5, r: row }; // Colonne F (index 5)

        const questionCellRef = XLSX.utils.encode_cell(questionCellAddress);
        const correctAnswerCellRef = XLSX.utils.encode_cell(correctAnswerCellAddress);

        const questionCell = worksheet[questionCellRef];
        const correctAnswerCell = worksheet[correctAnswerCellRef];

        if (questionCell && correctAnswerCell) {
            const questionText = questionCell.v;
            const correctAnswerValue = correctAnswerCell.v.toString();


            // Vérifier si c'est une question d'appariement
            if (correctAnswerValue.includes('-')) {
                let pairs = correctAnswerValue.split(',').map(pair => pair.split('-').map(num => parseInt(num.trim()) - 1));

                giftContent += `${questionText} {\n`;

                pairs.forEach(pair => {
                    const leftCol = 6 + pair[0];
                    const rightCol = 6 + pair[1];

                    const leftCellRef = XLSX.utils.encode_cell({ c: leftCol, r: row });
                    const rightCellRef = XLSX.utils.encode_cell({ c: rightCol, r: row });

                    const leftCell = worksheet[leftCellRef];
                    const rightCell = worksheet[rightCellRef];

                    if (leftCell && rightCell) {
                        // Séparer text1 et text2
                        const [text1, text2] = leftCell.v.split(' - ');
                        giftContent += ` =${text1.trim()} -> ${text2.trim()}\n`;
                    }
                });

                giftContent += `}\n\n`;

            } else
                // test pour une question ouverte
                if (correctAnswerValue == "") {
                    const answerCellAddress = { c: 6, r: row };
                    const answerCellRef = XLSX.utils.encode_cell(answerCellAddress);
                    const answerCell = worksheet[answerCellRef];

                    // Formater la question au format GIFT
                    giftContent += `${questionText} {\n`;
                    giftContent += ` =${answerCell.v}\n`;
                    giftContent += `}\n\n`;
                }
                else {
                    const correctAnswers = correctAnswerValue.split(',').map(a => parseInt(a.trim()) - 1); // Convertir en indices zéro-basés
                    let answers = [];
                    for (let col = 6; col <= range.e.c; ++col) { // Commencer à la colonne G (index 6)
                        const answerCellAddress = { c: col, r: row };
                        const answerCellRef = XLSX.utils.encode_cell(answerCellAddress);
                        const answerCell = worksheet[answerCellRef];

                        if (answerCell) {
                            answers.push(answerCell.v);
                        }
                    }

                    // Formater la question au format GIFT
                    giftContent += `${questionText} {\n`;
                    if (correctAnswers.length > 1) {
                        const correctPercentage = (100 / correctAnswers.length).toFixed(5);
                        answers.forEach((answer, index) => {
                            if (correctAnswers.includes(index)) {
                                giftContent += ` ~%${correctPercentage}%${answer}\n`;
                            } else {
                                giftContent += ` ~%0%${answer}\n`;
                            }
                        });
                    } else {
                        answers.forEach((answer, index) => {
                            if (correctAnswers.includes(index)) {
                                giftContent += ` =${answer}\n`;
                            } else {
                                giftContent += ` ~${answer}\n`;
                            }
                        });
                    }
                    giftContent += `}\n\n`;
                }
        }
    }
    // Créer un fichier texte et le télécharger
    const blob = new Blob([giftContent], { type: 'text/plain;charset=utf-8' });
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = 'questions_and_answers.txt';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
